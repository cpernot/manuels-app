package pernot.calixte.manuels

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import java.lang.NumberFormatException

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    fun next(v: View){
        val spinner:Spinner = findViewById(R.id.manuels_spinner)
        val index = spinner.selectedItemPosition
        try {
            val page: Int = findViewById<EditText>(R.id.page_entry).text.toString().toInt()
            val intent = Intent(this, ImageViewer::class.java).apply {
                putExtra("ManuelIndex", index)
                putExtra("Page", page)
            }
            startActivity(intent)
        }catch (nfe: NumberFormatException){
            return
        }
    }
}