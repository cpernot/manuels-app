package pernot.calixte.manuels

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import coil.ImageLoader
import coil.load
import coil.request.CachePolicy
import coil.request.Disposable
import com.jsibbold.zoomage.ZoomageView
import kotlinx.coroutines.*
import java.io.File
import java.net.URL

class ImageViewer : AppCompatActivity() {
    private lateinit var manuel: String
    private var page = 0
    private var loadingrequest: Disposable? = null
    private var loadingjob: Job? = null
    private lateinit var imgLoader :ImageLoader
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_viewer)
        imgLoader = ImageLoader.Builder(this)
            .crossfade(200)
            .diskCachePolicy(CachePolicy.DISABLED)
            .build()
        val manuelIndex = intent.getIntExtra("ManuelIndex", 0)
        manuel = resources.getStringArray(R.array.manuel_ids)[manuelIndex].lowercase()
        val manuelName = resources.getStringArray(R.array.manuel_names)[manuelIndex]
        page = intent.getIntExtra("Page", 1)
        title = "Manuel: $manuelName"
        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState)
        refresh()
        findViewById<EditText>(R.id.pagenumber).setOnEditorActionListener { textView, i, _ ->
            if (i == EditorInfo.IME_ACTION_DONE) {
                textView.clearFocus()
                jump(textView)
            }
            false
        }
    }
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        page = savedInstanceState.getInt("Page")
    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("Page", page)
    }
    private fun fetch(): File? {
        val name = "${manuel}_${page}"
        val file = File(cacheDir, name)
        if (!file.exists()) {
            try {
                val url = "https://ca.lixte.org/manuel/${manuel}/${page}"
                val data = URL(url).openStream().readBytes()
                file.createNewFile()
                file.writeBytes(data)
            } catch (e: Exception) {
                Log.e("Download Error", e.toString())
                return null
            }
        }
        return file
    }
    private fun refresh() {
        val imageview: ZoomageView = findViewById(R.id.pageview)
        val progress: ProgressBar = findViewById(R.id.progressbar)
        GlobalScope.launch {
            loadingjob?.cancel("Uzless sry")
            loadingjob?.join()
            loadingjob = null
            loadingrequest?.dispose()
            loadingrequest?.await()
            loadingrequest = null
            runOnUiThread {
                imageview.reset()
            }
            ensureActive()
            loadingjob = GlobalScope.launch {
                runOnUiThread {
                    findViewById<EditText>(R.id.pagenumber).setText(page.toString())
                    progress.visibility = View.VISIBLE
                }
                val file = fetch()
                ensureActive()
                loadingrequest = imageview.load(file, imgLoader)
                loadingrequest?.await()
                ensureActive()
                runOnUiThread {
                    progress.visibility = View.INVISIBLE
                }
            }.apply {
                invokeOnCompletion {
                    loadingjob = null
                }
                start()
            }
        }.start()
    }
    fun next(v: View) {
        page += 1
        refresh()
    }

    fun previous(v: View) {
        page -= 1
        refresh()
    }

    private fun jump(v: TextView) {
        val newpage = v.text.toString().toInt()
        if (newpage != page) {
            page = newpage
            refresh()
        }
    }
}